package com.example.mynewapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import kotlinx.android.synthetic.main.activity_main.*
import android.view.View
import android.graphics.Color
import android.widget.ArrayAdapter
import android.widget.ListView
class MainActivity : AppCompatActivity() {
    var someStrings = arrayOf<String>()
    var index: Int = 0
    //val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, someStrings)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        submitButton.setOnClickListener(object: View.OnClickListener
        {
            override fun onClick(view: View)
            {
                totalCals.text = cals.text
                remaining.text = cals.text
                //val cals: Editable = editText.text
                submitButton.visibility = View.INVISIBLE
                cals.visibility = View.INVISIBLE

            }
        })
        foodButton.setOnClickListener(object: View.OnClickListener
        {
            override fun onClick(view: View)
            {
                nCals.visibility = View.VISIBLE
                foodName.visibility = View.VISIBLE
                addFood.visibility = View.VISIBLE


            }
        })
        addFood.setOnClickListener(object: View.OnClickListener
        {
            override fun onClick(view: View)
            {
                val foodIn: String = foodName.text.toString()
                val cals: String = nCals.text.toString()
                val listItem: String = foodIn + " Calories: " +cals

                val tCals: String = totalCals.text.toString()
                if(tCals.toInt()<0)
                {
                    totalCals.setTextColor(Color.parseColor("#FF0000"))
                }
                nCals.visibility = View.INVISIBLE
                foodName.visibility = View.INVISIBLE
                addFood.visibility = View.INVISIBLE

                //someStrings[index] = listItem
               // index++
               /* val listView: ListView = findViewById(R.id.items)
                listView.adapter = adapter*/

                items.visibility = View.VISIBLE
                var remain: Int = remaining.text.toString().toInt()
                remain-=cals.toInt()
                remaining.text = remain.toString()

            }
        })
    }
}
